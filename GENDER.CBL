       IDENTIFICATION DIVISION.
       PROGRAM-ID. GENDER.
       AUTHOR. PEERAYA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "TICKET.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD.
      * เกี่ยวข้องกับการอ่านไฟล์
           05 TICKET-DATE.
              10 TICKET-DAY        PIC 9(2).
              10 FILLER            PIC X(1) VALUE '/'.
              10 TICKET-MONTH      PIC 9(2).
              10 FILLER            PIC X(1) VALUE '/'.
              10 TICKET-YEAR       PIC 9(4).
           05 FILLER               PIC X(1) VALUE ','.
           05 FILLER               PIC X(1) VALUE SPACE.
           05 TICKET-TIME          PIC X(5).
           05 FILLER               PIC X(1) VALUE ','.
           05 FILLER               PIC X(1) VALUE SPACE.
           05 TICKET-GENDER        PIC X(1).
           05 FILLER               PIC X(1) VALUE ','.
           05 FILLER               PIC X(1) VALUE SPACE.
           05 TICKET-AGE           PIC 9(3).
           05 FILLER               PIC X(55).

       WORKING-STORAGE SECTION. 
       01  WS-INPUT-FILE-STATUS    PIC X(2).
           88  FILE-OK             VALUE '00'.
           88  FILE-AT-END         VALUE '10'.
       01  WS-CALCULATION.
           05 WS-INPUT-COUNT       PIC 9(8).
           05 WS-TICKET.
      *    เกี่ยวข้องกับการ process
              10 WS-TICKET-DATE.
                 15 WS-TICKET-DAY        PIC 9(2).
                 15 WS-TICKET-MONTH      PIC 9(2).
                 15 WS-TICKET-YEAR       PIC 9(4).
              10 WS-TICKET-TIME          PIC X(5).
              10 WS-TICKET-GENDER        PIC X(1).
              10 WS-TICKET-AGE           PIC 9(3).
           05 WS-SUMMARY.
              10 WS-MALE                PIC 9(4) VALUE ZEROS.
              10 WS-FEMALE              PIC 9(4) VALUE ZEROS.

       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS  THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END  THRU 3000-EXIT
           GOBACK.

       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE
           IF FILE-OK 
              CONTINUE
           ELSE 
              DISPLAY '***** GENDER ABEND *****'
      *       ABEND = ABNORMAL END
              UPON CONSOLE 
      *       UPON CONSOLE >> SHOW REPORT ON ADMIN SCREEN
              DISPLAY '* PARA 1000-INITIAL FAIL *'
              UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS ' *'
              UPON CONSOLE 
              DISPLAY '***** GENDER ABEND *****'
              UPON CONSOLE 
              STOP RUN 
           END-IF 
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT 
           .
       1000-EXIT.
           EXIT.

       2000-PROCESS.
      *    DISPLAY 100-INPUT-RECORD 
           PERFORM 2100-MOVE-TICKET-DATA THRU 2100-EXIT
           PERFORM 2200-SUM-TOTAL-GENDER-TICKET THRU 2200-EXIT 
           PERFORM 8000-READ-FILE-INPUT THRU 8000-EXIT 
           .
       2000-EXIT.
           EXIT.

       2100-MOVE-TICKET-DATA.
           MOVE TICKET-GENDER      OF 100-INPUT-RECORD
              TO WS-TICKET-GENDER  OF WS-TICKET
           .
       2100-EXIT.
           EXIT.

       2200-SUM-TOTAL-GENDER-TICKET.
           EVALUATE WS-TICKET-GENDER  
              WHEN 'M'
                 ADD 1 TO WS-MALE 
              WHEN 'F'
                 ADD 1 TO WS-FEMALE 
           END-EVALUATE
           .
       2200-EXIT.
           EXIT.

       3000-END.
           CLOSE 100-INPUT-FILE 
           IF FILE-OK 
              CONTINUE
           ELSE 
              DISPLAY '***** GENDER ABEND *****'
              UPON CONSOLE 
              DISPLAY '* PARA 3000-END FAIL *'
              UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS ' *'
              UPON CONSOLE 
              DISPLAY '***** GENDER ABEND *****'
              UPON CONSOLE 
              STOP RUN 
           END-IF 
           DISPLAY 'M: ' WS-MALE 
           DISPLAY 'F: ' WS-FEMALE 
           .
       3000-EXIT.
           EXIT.

       8000-READ-FILE-INPUT.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD 1 TO WS-INPUT-COUNT 
           ELSE 
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY '***** GENDER ABEND *****'
                 UPON CONSOLE
                 DISPLAY '* PARA 8000-READ-FILE-INPUT FAIL *'
                 UPON CONSOLE 
                 DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS ' *'
                 UPON CONSOLE 
                 DISPLAY '***** GENDER ABEND *****'
                 UPON CONSOLE 
                 STOP RUN 
              END-IF 
           END-IF 
           .
       8000-EXIT.
           EXIT.
